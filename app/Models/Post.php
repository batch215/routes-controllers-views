<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    public function user(){
        //This means that a post has a realationship with a user and it belongs to a user
        //we could also say that a single post belongs to a user
        return $this->belongsTo('App\Models\User');
    }
}
